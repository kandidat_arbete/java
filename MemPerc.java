import java.lang.Math;
import java.util.ArrayList;

public class MemPerc {
  public final boolean infinite;
  public final Node topNode = new Node();
  public final Node bottomNode = new Node();
  private final int size;
  private final double lambda;

  public MemPerc(int size, double lambda) {
    this.size = size;
    this.lambda = lambda;

	ArrayList<ArrayList<Node>> lastRow = null;

    for (int y = 0; y < size; y++) {
		ArrayList<ArrayList<Node>> currentRow = new ArrayList<>(size);
		boolean connected = false;

		for (int x = 0; x < size; x++) {

			final int k = Poisson(lambda * 4); //Area of a region is 4.
			ArrayList<Node> region = new ArrayList<Node>(k);

			for (int j = 0; j < k; j++) {
				Node n = new Node((x + Math.random())*2, (y + Math.random())*2,
						topNode, bottomNode, 2*size - 1);

				if (lastRow != null) {
					if (x > 0)
						n.checkNeighbours(lastRow.get(x-1));
					n.checkNeighbours(lastRow.get(x));
					if (x < size-1)
						n.checkNeighbours(lastRow.get(x+1));
				}

				if (x > 0)
					n.checkNeighbours(currentRow.get(x-1));
				n.checkNeighbours(region);

				connected = connected || (n.findRoot() == topNode.findRoot());
				region.add(j, n);
			}

			currentRow.add(x, region);
		}
		if (!connected) {
			this.infinite = false;
			return;
		}
		lastRow = currentRow;
    }

    this.infinite = (topNode.findRoot() == bottomNode.findRoot());
  }

  private int Poisson(double l) {
    final double L = Math.exp(-l);
    int k = 0;
    double p = 1;

    while (p > L) {
      k++;
      p *= Math.random();
    }
    return k - 1;
  }
}
