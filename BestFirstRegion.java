import java.lang.Math;
import java.util.ArrayList;
import java.util.Random;

public class BestFirstRegion {
	public final static double region_length = Math.sqrt(2);
	public final static double region_area = 2;
	public final ArrayList<Coordinate> list;
	private BestFirstPerc perc;
	private Random generator;
	private int k;

	private class Coordinate {
		public final double x, y;
		public Coordinate(double x, double y) {
			this.x = x;
			this.y = y;
		}
	}

	public BestFirstRegion(int x, int y, BestFirstPerc perc) {
		this.perc = perc;
		generator = new Random(perc.seed ^ x);
		generator.setSeed(generator.nextLong() ^ y);
		k = Poisson(perc.lambda * region_area);
		list = new ArrayList<Coordinate>(k);

		for (int i = 0; i < k; i++) {
			list.add(i, new Coordinate((x + generator.nextDouble()) * region_length,
						               			 (y + generator.nextDouble()) * region_length));
		}
		generator = null;
	}

	public boolean overlaps(int other_x, int other_y) {
		return overlaps(new BestFirstRegion(other_x, other_y, perc));
	}

	public boolean overlaps(BestFirstRegion other) {
		for (Coordinate c : other.list) {
			for (Coordinate d : list) {
				if ((c.x - d.x)*(c.x - d.x) + (c.y - d.y)*(c.y - d.y) < 4)
					return true;
			}
		}
		return false;
	}

	private int Poisson(double l) {
	  int x = 0;
	  double p = Math.exp(-l);
	  double s = p;

	  double u = generator.nextDouble();
	  while (u > s) {
		  x++;
		  p *= l/x;
		  s += p;
	  }
	  return x;
  }

}
