import java.util.BitSet;
import java.util.ArrayDeque;

public class DepthFirstPerc {
	private final BitSet added;
	private final ArrayDeque<Coordinate> neighbours = new ArrayDeque<>();
	public final boolean infinite;
	public final long seed;
	public final double lambda;
	public final int size;

	private class Coordinate {
		public final int x, y;
		Coordinate(int x, int y) {
			this.x = x;
			this.y = y;
		}
		int index() {
			return y*size + x;
		}
	}

	public static void main(String[] args) {
		int size = Integer.parseInt(args[0]);
		double lambda = Double.parseDouble(args[1]);
		long seed = 0;
		if (args.length > 2) {
			seed =  Long.parseLong(args[2]);
		} else {
			seed = System.currentTimeMillis();
		}

		DepthFirstPerc p = new DepthFirstPerc(size, lambda, seed);
		System.out.println("Seed: " + seed + ". Lambda: " + lambda + 
				". Result: " + p.infinite);
	}

	public DepthFirstPerc(int size, double lambda, long seed) {
		this.size = size;
		this.lambda = lambda;
		this.seed = seed;
		added = new BitSet(size*size);

		//Adds the upper row to neighbours
		for (int x = 0; x < size; x++) {
			Coordinate c = new Coordinate(x, 0);
			neighbours.add(c);
			added.set(c.index());
		}

		//Depth first search
		while (!neighbours.isEmpty()) {
			Coordinate c = neighbours.pop();
			if (c.y == size -1) {
				this.infinite = true;
				return;
			}

			DepthFirstRegion region = new DepthFirstRegion(c.x, c.y, this);

			for (int x = -2; x < 3; x++) {
				for (int y = -2; y < 3; y++) {
					Coordinate d = new Coordinate(c.x + x, c.y + y);
					if (d.x >= 0 && d.y >= 0 && d.x < size && d.y < size &&
							!added.get(d.index()) && region.overlaps(c.x + x, c.y + y)) {
						neighbours.push(d);
						added.set(d.index());
					}
				}
			}
		}
		this.infinite = false;
		return;
	}



}
