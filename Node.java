import java.util.List;
import java.lang.Math;

public class Node {
  public final double x;
  public final double y;
  public Node parent;
  public int rank;

  public Node() {
    //This is not a Node on the board.
    this.x = -1;
    this.y = -1;
    this.rank = 0;
    this.parent = this;
  }

  public Node(double x, double y, Node Top, Node Bottom, int edge) {
    this.x = x;
    this.y = y;
    this.rank = 0;
    this.parent = this;

    if (y < 1) {
      connect(Top);
    } else if (y > edge) {
      connect(Bottom);
    }
  }

  public void checkNeighbours(List<Node> list) {
    for (Node n : list) {
      if (n != null && (n.x - x)*(n.x - x) + (n.y - y)*(n.y - y) < 4) {
        connect(n);
      }
    }
  }

  private void connect(Node n) {
    Node myRoot = findRoot();
    Node otherRoot = n.findRoot();

    if (myRoot == otherRoot) {
      return;
    }

    if (myRoot.rank < otherRoot.rank) {
      myRoot.parent = otherRoot;
    } else if (myRoot.rank > otherRoot.rank) {
      otherRoot.parent = myRoot;
    } else {
      otherRoot.parent = myRoot;
      myRoot.rank += 1;
    }
  }

  public Node findRoot() {
    if (this != this.parent) {
      this.parent = this.parent.findRoot();
    }
    return this.parent;
  }
}
