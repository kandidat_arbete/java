import java.lang.Math;

public class Statistic {
	public static void main(String[] args) {
		new Statistic(Integer.parseInt(args[0]), Double.valueOf(args[1]),
				Integer.parseInt(args[2]));
	}

	public Statistic(int size, double lambda, int times) {
		int dsum = 0, msum = 0;
		int dsize = (int)Math.ceil((size + 0.83)/1.41);
		int msize = (int)Math.ceil(size/2.0);
		
		for (int i = 0; i < times; i++) {
			DepthFirstPerc d = new DepthFirstPerc(size, lambda, System.currentTimeMillis());
			MemPerc m = new MemPerc(size, lambda);
			dsum += d.infinite ? 1 : 0;
			msum += m.infinite ? 1 : 0;
			System.out.printf("%d/%d (%3.1f %%) Current mem-result: %3.1f %% Current dfs-result: %3.1f %%%n",
					i + 1, times, (i + 1) * 100.0 / times, msum * 100.0 / (i + 1), dsum * 100.0 / (i + 1));
		}

		System.out.printf("%nJob done! java Statistic %d %.5f %d resulted in:%n",
				size, lambda, times);
		System.out.printf("%d (mem) and %d (dfs) of %d had infinite clusters.%n", msum, dsum, times);
	}
}
