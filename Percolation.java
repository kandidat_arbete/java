import java.lang.Math;
import java.util.ArrayList;

public class Percolation {
  public final boolean infinite;
  public final Node topNode = new Node();
  public final Node bottomNode = new Node();
  private final int size;
  private final double lambda;
  public final ArrayList<ArrayList<Node>> regionList;

  public Percolation(int size, double lambda) {
    this.size = size;
    this.lambda = lambda;

    this.regionList = new ArrayList<>(size*size);

    for (int i = 0; i < size*size; i++) {
      int x = getX(i);
      int y = getY(i);

      final int k = Poisson(lambda * 4); //Area of a region is 4.
      ArrayList<Node> region = new ArrayList<Node>(k);

      for (int j = 0; j < k; j++) {
        Node n = new Node((x + Math.random())*2, (y + Math.random())*2,
            topNode, bottomNode, 2*size - 1);

        n.checkNeighbours(getRegion(x-1, y-1));
        n.checkNeighbours(getRegion(x, y-1));
        n.checkNeighbours(getRegion(x+1, y-1));
        n.checkNeighbours(getRegion(x-1, y));

        n.checkNeighbours(region);
        region.add(j, n);
      }

      regionList.add(i, region);
    }

    this.infinite = (topNode.findRoot() == bottomNode.findRoot());
  }

  private int getX(int i) {
    return i % size;
  }

  private int getY(int i) {
    return i / size;
  }

  private ArrayList<Node> getRegion(int x, int y) {
    if (x < 0 || x >= size || y < 0 || y >= size) {
      return new ArrayList<Node>(0);
    }

    return regionList.get(y * size + x);
  }

  private int Poisson(double l) {
	  int x = 0;
	  double p = Math.exp(-l);
	  double s = p;

	  double u = Math.random();
	  while (u > s) {
		  x++;
		  p *= l/x;
		  s += p;
	  }
	  return x;
  }
}
