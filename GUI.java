import javax.swing.JFrame;
import java.util.ArrayList;
import java.awt.Graphics;
import java.awt.Color;

public class GUI extends JFrame {
  private final Percolation p;
  private final int size;

  public static void main(String[] args) {
    final int size = Integer.parseInt(args[0]);
    final double lambda = Double.valueOf(args[1]);

    Percolation p = new Percolation(size, lambda);
    new GUI(size, p);
  }

  public GUI (int size, Percolation p) {
    super( p.infinite ? "Oändlig" : "Ändlig" );

    this.p = p;
    this.size = size;

    setSize(1000, 1000);
    setLayout(null);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setVisible(true);
  }

  public void paint(Graphics g) {
    super.paint(g);

    g.setColor(Color.BLACK);
    g.fillRect(0, 0, 1000, 1000);

    for (ArrayList<Node> list : p.regionList) {
      for (Node n : list) {
        int x = (int) (n.x / size * 500);
        int y = (int) (n.y / size * 500);

        boolean connTop = (n.findRoot() == p.topNode.findRoot());
        boolean connBot = (n.findRoot() == p.bottomNode.findRoot());

        if (connTop && connBot) {
          g.setColor(Color.GREEN);
        } else if (connTop) {
          g.setColor(Color.BLUE);
        } else if (connBot) {
          g.setColor(Color.RED);
        } else {
          if (size >= 500) {
            continue;
          } else {
            g.setColor(Color.WHITE);
          }
        }

        if (size >= 500) {
          g.drawRect(x, y, 1, 1);
        } else if (size > 100) {
          int radius = 500/size;
          g.fillOval(x - radius , y - radius, 2*radius, 2*radius);
        } else {
          int radius = 500/size;
          g.drawOval(x - radius , y - radius, 2*radius, 2*radius);
          g.drawRect(x, y, 1, 1);
        }
      }
    }
  }
}
